require 'spec_helper'

describe Core::API::Entities::Error do
  let(:error) { 'error' }
  let(:options) { {} }
  let(:instance_class) { described_class }
  let(:instance) { instance_class.new error, options }

  subject{ instance.as_json }

  it 'expect to have boolean success field' do
    expect(subject[:success]).to be_falsey
  end

  it 'expect to have error text in error field' do
    expect(subject[:errors]).to eq [error]
  end

  context 'with nested error object' do
    let(:error) { { error: 'error' } }
    let(:instance_class) do
      klass = Class.new described_class
      klass.errors do |object, options|
        object[:error]
      end
      klass
    end
    it 'expect to fetch error from nested object' do
      expect(subject[:errors]).to eq ['error']
    end
  end

  context 'without backtrace' do
    it 'expect not have backtrace property' do
      expect(subject.has_key? :backtrace).to be_falsey
    end
  end

  context 'with backtrace' do
    let(:options) { { backtrace: ['backtrace'] } }
    let(:instance_class) do
      klass = Class.new described_class
      klass.backtrace { |object, options| options[:backtrace] }
      klass
    end
    it 'expect to fetch error backtrace' do
      expect(subject[:backtrace]).to eq ['backtrace']
    end
  end
end