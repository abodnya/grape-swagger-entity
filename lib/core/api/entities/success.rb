class Core::API::Entities::Success < Core::API::Entity
  expose :success, documentation: { type: 'Boolean', required: true, desc: 'Successfully value' }
  private
  def success; true end
end