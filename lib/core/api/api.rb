require 'grape'
require 'grape-entity'
require 'grape-swagger/entity'
require 'grape-kaminari'

module Core::API
  extend ActiveSupport::Autoload
  autoload :Root
  eager_autoload do
    autoload :Entities
    autoload :Entity
    autoload :Helper
    autoload :Resources
    autoload :Middleware
  end
end