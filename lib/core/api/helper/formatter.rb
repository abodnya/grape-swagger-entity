module Core::API::Helper::Formatter
  require_relative 'formatter/error_formatter'
  require_relative 'formatter/json_formatter'
  extend ActiveSupport::Concern
  included do
    format :json
    default_format :json
    error_formatter :json, Core::API::Helper::Formatter::ErrorFormatter
    formatter :json, Core::API::Helper::Formatter::JsonFormatter

    class << self
      # Define response entity with description, status code, etc.
      # Examples:
      # entity :entity_name, 200, 'Awesome entity', Grape::Entity, { is_array: true }
      # entity :entity_name, 200, 'Awesome entity', Grape::Entity
      # entity :entity_name, 'Awesome entity', Grape::Entity
      # entity :entity_name, { code: 200 }
      # entity { code: 200, key: :entity, klass: Grape::Entity, desc: 'Awesome entity' }
      def entity(*args)
        key,
            code,
            desc,
            klass,
            options = case args.length
                        when 5 # Example :entity_name, 200, 'Awesome entity', Grape::Entity, { is_array: true }
                          args
                        when 4 # Example :entity_name, 200, 'Awesome entity', Grape::Entity
                          [args[0], args[1], args[2], args[3]]
                        when 3 # Example :entity_name, 'Awesome entity', Grape::Entity
                          [args.first, 200, args[1], args.last]
                        when 2 # Example :entity_name, { code: 200 }
                          [args.first, 200, '', nil, args.last]
                        when 1 # Example { code: 200, key: :entity, klass: Grape::Entity, desc: 'Awesome entity' }
                          if args.first.is_a? Hash
                            [nil, 200, '', nil, args.first]
                          else
                            [args.first, 200, '', nil, {}]
                          end
                        when 0
                          [nil, 200, '', nil, {}]
                      end
        entity = {
            key: key,
            code: code,
            desc: desc,
            klass: klass
        }.merge(options || {})

        if(Core::API::Entities.grape_entity? entity.fetch(:klass, Object))
          route_setting :raw_entity, entity
          entity_http entity
        end
      end

      def entity_http(entity)
        # entity = route_setting :entity
        http_code entity[:code], entity[:desc], Core::API::Entities.fetch_entity(self)
      end
    end



    # Define default entity presenter
    self.global_route_start do
      self.entity({ code: 200,
                    desc: 'Success action',
                    klass: Core::API::Entities::Success
                  }) unless route_setting(:raw_entity)
    end
  end
end
