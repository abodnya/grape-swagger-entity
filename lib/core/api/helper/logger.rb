module Core::API::Helper::Logger
  extend ActiveSupport::Concern
  included do
    use API::Middleware::Logger
  end
end