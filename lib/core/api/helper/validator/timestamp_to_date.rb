class Core::API::Helper::Validator::TimestampToDate < Grape::Validations::Base
  def validate_param!(attr_name, params)
    date = params[attr_name]
    if date.present?
      params[attr_name] = date.kind_of?(String) ? Date.strptime(date, "%m/%d/%Y") : Time.at(date).utc.to_date
    end
  end
end