module Core::API::Helper::Exceptions
  extend ActiveSupport::Concern
  included do
    rescue_from Core::Service::Exception::Data do |e|
      error!(e.message, 400)
    end

    rescue_from Core::Service::Exception::Authorization do |e|
      error!(e.message, 401)
    end

    rescue_from Core::Service::Exception::Permissions do |e|
      error!(e.message, 403)
    end

    rescue_from Core::Service::Exception::Record do |e|
      error!(e.record, 422)
    end

    rescue_from :all
    helpers do
      def need_to_implement!
        raise 'need to implement'
      end
    end
  end
end