module Core::API::Helper::Desc
  extend ActiveSupport::Concern
  included do
    # TODO: Refactor this methods
    class << self
      def headers(headers)
        config = self.desc_container
        config.configure do
          headers headers
        end
        namespace_setting :description, (route_setting(:description) || {}).merge(config.settings)
        route_setting :description, (route_setting(:description) || {}).merge(config.settings)

      end

      def http_code(code, desc, entity)
        codes = description_field(:http_codes) || []
        codes << [code, desc, entity]
        description_field(:http_codes, codes)
      end
    end
  end
end