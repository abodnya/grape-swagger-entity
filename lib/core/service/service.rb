module Core::Service
  extend ActiveSupport::Autoload
  eager_autoload do
    autoload :Exception
    autoload :Base
  end
end