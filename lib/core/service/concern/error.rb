module Core::Service::Error
  extend ActiveSupport::Concern

  included do
    delegate :authorization_error!, :permissions_error!, :data_error!, :record_error!, :check_record!, to: self
  end

  class_methods do
    def data_error!(message)
      raise Core::Service::Exception::Data.new(message)
    end

    def authorization_error!(message)
      raise Core::Service::Exception::Authorization.new(message)
    end

    def permissions_error!(message)
      raise Core::Service::Exception::Permissions.new(message)
    end

    def record_error!(record, data = nil)
      error = case record
                when String, Symbol
                  { record => data.is_a?(Array) ? data : [data] }
                when Hash
                  record
                when ActiveRecord::Base
                  record.errors
              end
      raise Core::Service::Exception::Record.new(error)
    end

    def check_record!(record)
      record.valid? ? record : record_error!(record)
    end
  end
end