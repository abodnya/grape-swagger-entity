module Core::Service::Authorized
  extend ActiveSupport::Concern

  included do
    attr_accessor :current_user
  end

  def initialize(current_user = nil)
    @current_user = current_user
  end
end